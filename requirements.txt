asgiref==3.3.4
Django==3.2.4
djangorestframework==3.12.4
drf-jwt==1.19.1
pytz==2021.1
sqlparse==0.4.1
redis==3.5.3
celery==4.4.7
psycopg2-binary==2.8.6
flower==0.9.7
requests==2.25.1
xmltodict==0.12.0
flake8==3.9.2
django-cors-headers==3.7.0
django-utils-six==2.0
